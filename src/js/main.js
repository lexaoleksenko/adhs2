const btn = document.querySelector(".navbar-phone-btn")
const navbar = document.querySelector(".navbar")


btn.addEventListener("click", open)

function open() {
    navbar.classList.toggle("navbar_none")
    btn.classList.toggle("navbar-phone-btn_x")
}