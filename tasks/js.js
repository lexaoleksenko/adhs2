import gulp from "gulp";

// Конфигурация
import path from "../config/path.js";
import app from "../config/app.js";

// Плагины
import plumber from "gulp-plumber";
import notify from "gulp-notify";
import babel from "gulp-babel";
import uglify from "gulp-uglify";

// Обработка JS
export default () => {
    return gulp.src(path.js.src, {sourcemaps: app.isDev})
    .pipe(plumber({
        errorHandler: notify.onError(error => ({
            title: "JavaScript",
            message: error.message
        }))
    }))
    .pipe(babel())
    .pipe(uglify())
    .pipe(gulp.dest(path.js.dest, {sourcemaps: app.isDev}));
}







// Маски выбора файлов: 
// Если нам нужно найти все HTML - return src("./src/html/*.html") 
// Если нужно найти все файлы return src("./src/html/*.*") 
// Если нужны конкретные return src("./src/html/*.{html,css}") 
// Так же работает и с директориями return src("./src/{html,css}/*.{html.css}") 
// Поиск всех файлов в директории src вне зависимости от их вложенности return src("./src/**/*.*"))

